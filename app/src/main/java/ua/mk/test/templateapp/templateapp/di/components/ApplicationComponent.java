package ua.mk.test.templateapp.templateapp.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ua.mk.test.templateapp.templateapp.App;
import ua.mk.test.templateapp.templateapp.di.modules.ApplicationModule;
import ua.mk.test.templateapp.templateapp.navigation.di.NavigationModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NavigationModule.class
})
public interface ApplicationComponent {

    //Navigation
    NavigatorHolder navigatorHolder();

    Router router();

    void inject(App app);
}
