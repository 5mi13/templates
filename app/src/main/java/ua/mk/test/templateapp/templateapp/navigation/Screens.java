package ua.mk.test.templateapp.templateapp.navigation;

public class Screens {
    public static final String SALOON_LIST_SCREEN = "saloons";

    public static final String SEARCH_SCREEN = "search";

    public static final String PROFILE_SCREEN = "profile";
}
