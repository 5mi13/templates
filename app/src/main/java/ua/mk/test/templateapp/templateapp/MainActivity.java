package ua.mk.test.templateapp.templateapp;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import ua.mk.test.templateapp.templateapp.presentation.main.MainPresenter;
import ua.mk.test.templateapp.templateapp.presentation.main.MainScreenOpenParams;
import ua.mk.test.templateapp.templateapp.presentation.main.MainView;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    private static final String TAG = MainActivity.class.getName();

    @Inject
    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        processIntent(getIntent());
    }

    private void processIntent(Intent intent) {
        MainScreenOpenParams openParams = MainScreenOpenParams.fromBundle(intent.getExtras());
       // presenter.navigateTo(openParams);
    }

    @ProvidePresenter
    public MainPresenter createPresenter() {
        return presenter;
    }

    @Override
    public void selectInBottomNavigationMenu(int screenId) {
        // TODO(dkovtun, 04.05.2018): select icon in navigation.
    }
}
