package ua.mk.test.templateapp.templateapp.navigation.main;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.Objects;

import ua.mk.test.templateapp.templateapp.R;
import ua.mk.test.templateapp.templateapp.navigation.Screens;

public class MainNavigationRouteFabric {
    private static final SparseArray<String> routeMap;

    static {
        routeMap = new SparseArray<>();
        routeMap.put(R.id.navigation_saloon, Screens.SALOON_LIST_SCREEN);
        routeMap.put(R.id.navigation_search, Screens.SEARCH_SCREEN);
        routeMap.put(R.id.navigation_profile, Screens.PROFILE_SCREEN);
    }

    private MainNavigationRouteFabric() {
    }

    @NonNull
    public static String getScreenKey(@IdRes int menuItemId) {
        return Objects.requireNonNull(routeMap.get(menuItemId));
    }

    @IdRes
    public static int getScreenId(String screen) {
        for (int i = 0; i < routeMap.size(); i++) {
            String value = routeMap.valueAt(i);
            if (value.equals(screen)) {
                return routeMap.keyAt(i);
            }
        }

        return 0;
    }
}
