package ua.mk.test.templateapp.templateapp.di.manager;

import android.support.annotation.NonNull;

public interface ComponentProvider {
    @NonNull
    <T> T getComponent(Class<T> clazz);
}
