package ua.mk.test.templateapp.templateapp;

import android.app.Application;

import ua.mk.test.templateapp.templateapp.di.components.ApplicationComponent;
import ua.mk.test.templateapp.templateapp.di.manager.ComponentManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        inject();
    }

    private void inject() {
        ComponentManager.init(this);
        ApplicationComponent appComponent = ComponentManager.getInstance()
                .getApplicationComponent();
        appComponent.inject(this);
    }
}
