package ua.mk.test.templateapp.templateapp.presentation.main;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Objects;

public class MainScreenOpenParams {
    private static final String EXTRA_SCREEN_KEY = "main_screen_open_params_screen_key";
    private static final String EXTRA_SCREEN_PARAM = "main_screen_open_params_screen_param";

    private final String subscreenKey;
    @Nullable
    private final Parcelable screenParam;

    public MainScreenOpenParams(String subscreenKey) {
        this(subscreenKey, null);
    }

    public MainScreenOpenParams(String subscreenKey,
                                @Nullable Parcelable screenParam) {
        this.subscreenKey = subscreenKey;
        this.screenParam = screenParam;
    }

    @Nullable
    public static MainScreenOpenParams fromBundle(Bundle bundle) {
        if (bundle == null || !bundle.containsKey(EXTRA_SCREEN_KEY)) {
            return null;
        }

        String screenKey = bundle.getString(EXTRA_SCREEN_KEY);
        Parcelable screenParam = bundle.getParcelable(EXTRA_SCREEN_PARAM);

        return new MainScreenOpenParams(screenKey, screenParam);
    }

    public void toBundle(@NonNull Bundle bundle) {
        Objects.requireNonNull(bundle);

        bundle.putString(EXTRA_SCREEN_KEY, subscreenKey);

        if (screenParam != null) {
            bundle.putParcelable(EXTRA_SCREEN_PARAM, screenParam);
        }
    }

    public String getSubscreenKey() {
        return subscreenKey;
    }

    @Nullable
    public Parcelable getScreenParam() {
        return screenParam;
    }
}
