package ua.mk.test.templateapp.templateapp.presentation.main;

import com.arellomobile.mvp.MvpView;

public interface MainView extends MvpView {
    void selectInBottomNavigationMenu(int screenId);
}
