package ua.mk.test.templateapp.templateapp.presentation.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ua.mk.test.templateapp.templateapp.navigation.Screens;
import ua.mk.test.templateapp.templateapp.navigation.main.MainNavigationRouteFabric;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private final Router router;
    private String currentScreen;

    @Inject
    MainPresenter(Router router){
        this.router=router;
    }

    public void navigateTo(MainScreenOpenParams openParams) {
        if (openParams == null) {
            openParams = new MainScreenOpenParams(Screens.SALOON_LIST_SCREEN);
        }
        String screenKey = openParams.getSubscreenKey();
        if (!screenKey.equals(currentScreen)) {
            currentScreen = screenKey;
            getViewState().selectInBottomNavigationMenu(MainNavigationRouteFabric.getScreenId(screenKey));
        }
    }
}
